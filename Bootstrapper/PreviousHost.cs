﻿
namespace Bootstrapper
{
    /// <summary>
    /// Object containing the last connected server saved in Config JSON
    /// </summary>
    public class PreviousHost
    {
        public Server Server { get; set; }
    }
}
